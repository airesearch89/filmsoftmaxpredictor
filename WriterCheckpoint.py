import logging
import os
import shutil
from datetime import datetime

import xlsxwriter


class WriterCheckpoint:
    name_mode = None
    date_after_month = None

    def __init__(self):
        logging.info("create checkpoint")
        self.date_after_month = datetime.now()
        self.name_model = "my_model." + self.date_after_month.strftime('%Y.%m.%d_%H.%M.%S')

    def write_to_word(self, y_test, result, keywords_text, content, starter_learning_rate, count_epoch):
        workbook = xlsxwriter.Workbook('./xlsx/' + self.name_model + '.xlsx')
        worksheet1 = workbook.add_worksheet()
        worksheet1.write_string(0, 1, 'номер')
        worksheet1.write_string(0, 2, 'Верный ответ:\t')
        worksheet1.write_string(0, 3, 'Ответ НС:\t')
        worksheet1.write_string(0, 4, 'Направление')
        worksheet1.write_string(0, 5, 'Значение:\t')
        count_points = 50
        for ii, temp_test in enumerate(y_test):
            worksheet1.write_number(ii + 1, 1, int(ii + 1))
            worksheet1.write_number(ii + 1, 2, temp_test[0])
            worksheet1.write_number(ii + 1, 3, float(result[ii][0]))
            worksheet1.write_formula(ii + 1, 4, '=if(or(and(C' + str(ii + 2) + '>0,D' + str(ii + 2) + '>0),and(C' + str(
                ii + 2) + '<0,D' + str(ii + 2) + '<0)),1,0)')
            worksheet1.write_formula(ii + 1, 5, '=if(and(E' + str(ii + 2) + '<2,ABS(ABS(C' + str(ii + 2) + '-D' + str(
                ii + 2) + '))<1/100),1,0)')
            worksheet1.write_formula(ii + 1, 6, '=if(F' + str(ii + 2) + ',1,if(E' + str(ii + 2) + ',1/2,0))')
            if ((ii + 1) % count_points) == 0:
                chart = workbook.add_chart({'type': 'line'})
                chart.add_series({'values': '=Sheet1!$c$' + str(ii + 3 - count_points) + ':$c$' + str(ii + 1)})
                chart.add_series({'values': '=Sheet1!$d$' + str(ii + 3 - count_points) + ':$d$' + str(ii + 1)})
                chart.width = chart.width * 2
                chart.height = chart.height * 2
                worksheet1.insert_chart('H' + str(ii + 3 - count_points), chart)

        def write_excel_end():
            chart2 = workbook.add_chart({'type': 'line'})
            chart2.add_series({'values': '=Sheet1!$c$2:$c$' + str(ii + 1)})
            chart2.add_series({'values': '=Sheet1!$d$2:$d$' + str(ii + 1)})
            chart2.width = chart2.width * 2
            chart2.height = chart2.height * 2
            worksheet1.insert_chart('I' + str(ii + 1), chart2)
            worksheet1.write_formula(ii + 2, 4, '=sum(E2:E' + str(ii + 2) + ')')
            worksheet1.write_formula(ii + 2, 5, '=sum(F2:F' + str(ii + 2) + ')')
            worksheet1.write_formula(ii + 2, 6, '=sum(G2:G' + str(ii + 2) + ')')
            percent_format = workbook.add_format({'num_format': '0.0%'})
            worksheet1.write_formula(ii + 3, 4, '=E' + str(ii + 3) + '/B' + str(ii + 2), percent_format)
            worksheet1.write_formula(ii + 3, 5, '=F' + str(ii + 3) + '/B' + str(ii + 2), percent_format)
            worksheet1.write_formula(ii + 3, 6, '=G' + str(ii + 3) + '/B' + str(ii + 2), percent_format)
            worksheet1.write_formula(ii + 3, 7, '=correl(c2:c' + str(ii + 2) + ',d2:d' + str(ii + 2) + ')')

            worksheet1.write_string(ii + 4, 2, 'keywords_text')
            worksheet1.write_string(ii + 4, 3, 'content')
            worksheet1.write_string(ii + 4, 4, 'starter_learning_rate')
            worksheet1.write_string(ii + 4, 5, 'count_epoch')

            worksheet1.write_string(ii + 5, 2, str(keywords_text))
            worksheet1.write_string(ii + 5, 3, str(content))
            worksheet1.write_string(ii + 5, 4, str(starter_learning_rate))
            worksheet1.write_string(ii + 5, 5, str(count_epoch))

            workbook.close()

        write_excel_end()

        print('yra______________________________________________')

    def save_checkpoint(self):
        shutil.copy2(r'PredictorFilm.py', r'./temp3/' + self.name_model)

    def save_mpdel(self, model):
        os.makedirs('checkpoints/' + self.name_model)
        model.save('checkpoints/' + self.name_model + '/' + self.name_model + '.tflearn')

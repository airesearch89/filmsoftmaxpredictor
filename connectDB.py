import logging
import time
from calendar import timegm

import mysql.connector

from model.film import *


class ConnectionDB:
    cnx = None

    def __init__(self):
        logging.info("start_connection")
        self.cnx = mysql.connector.connect(user='root', password='root',
                                           host='localhost',
                                           database='film')
        logging.info("end_connection")

    def __del__(self):
        logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                            level=logging.INFO)
        logging.info("start_disconnection")
        if (self.cnx != None):
            self.cnx.close()
        logging.info("end_disconnection")

    def twitter_utc_date_to_unix_date(self, iso_string):
        return timegm(
            time.strptime(
                iso_string.replace('Z', 'GMT'),
                '%a %b %d %H:%M:%S +0000 %Y'
            )
        )

    def getattribute(self, name: str):
        return super().__getattribute__(name)

    def get_publications(self, count, is_test=False):

        list_atributes = 'id, actors, country, description, name, producer, rating, year, user_rating'
        sql_list_atributes = "film.film.id, film.film.actors, film.film.country, film.film.description, film.film.name," \
                             " film.film.producer, film.film.rating, film.film.year, film.film.user_rating"

        key = None
        if is_test:
            key = " is null or film.film.user_rating=0"
        else:
            key = ">0"
        query = "SELECT " + sql_list_atributes + " FROM film.film where film.film.user_rating" + key
        try:
            publications = []
            cursor = self.cnx.cursor()
            cursor.execute('SET NAMES utf8mb4')
            cursor.execute(query)
            for (line) in cursor:
                publications.append(Film(list_atributes.split(", "), line))
            return publications

        except Exception as error:
            logging.error(error)
            return None
        finally:
            cursor.close()

    def update_rating(self, id, rating):
        if(id!=None and rating!=None and id !="" and rating!=""):
            try:
                cursor = self.cnx.cursor()
                cursor.execute('SET NAMES utf8mb4')
                query='UPDATE `film`.`film` SET answerns='+str(rating)+' WHERE id="'+str(id)+'"'
                cursor.execute(query)
                self.cnx.commit()
                return id
            except Exception as error:
                logging.error(error)
                return None
            finally:
                cursor.close()

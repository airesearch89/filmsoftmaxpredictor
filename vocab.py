import re
import sys
from collections import Counter

from nltk.stem.snowball import RussianStemmer
from nltk.tokenize import TweetTokenizer


class Vocab:
    stemer = None
    regex = None
    stem_count = None
    stem_cache = None
    tokenizer = None
    vocab = None
    token_2_idx = None

    def get_stem(self, token):
        stem = self.stem_cache.get(token, None)
        if stem:
            return stem
        token = self.regex.sub('', token).lower()
        stem = self.stemer.stem(token)
        self.stem_cache[token] = stem
        return stem

    def count_unique_tokens_in_tweets(self, tweets, column_name):
        for i, content in enumerate(tweets):
            sys.stdout.write("\rcreate " + column_name + " vocab: %d%%" % (i / len(tweets) * 100))
            sys.stdout.flush()
            attribut = content.getattribute(column_name)
            tokens = self.tokenizer.tokenize(attribut)
            for token in tokens:
                stem = self.get_stem(token)
                self.stem_count[stem] += 1

    def create_vocab(self, VOCAB_SIZE, publications, column_name):
        self.stemer = RussianStemmer()
        self.regex = re.compile('[^а-яА-Яa-zA-Z0-9]')
        self.stem_cache = {}
        self.stem_count = Counter()
        self.tokenizer = TweetTokenizer()
        print("start\n")
        self.count_unique_tokens_in_tweets(publications, column_name)
        self.vocab = sorted(self.stem_count, key=self.stem_count.get, reverse=True)[:VOCAB_SIZE]
        self.token_2_idx = {self.vocab[i]: i for i in range(VOCAB_SIZE)}
        sys.stdout.write(
            "\rcreate " + column_name + " vocab: 100%%. Total unique stems found: " + str(len(self.stem_count)) + "\n")

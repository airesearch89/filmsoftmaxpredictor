import numpy as np

from model.base_model import BaseModel


class Film(BaseModel):
    id = None
    actors = None
    country = None
    description = None
    href = None
    name = None
    producer = None
    rating = None
    year = None
    answer_ns = None
    user_rating = None

    def to_vector(self, vocab_dict, show_unknowns=False):
        'id, actors, country, description, name, producer, rating, year, user_rating'
        full_len = len(vocab_dict['actors'].vocab) + len(vocab_dict['country'].vocab) + len(
            vocab_dict['description'].vocab) + +len(vocab_dict['name'].vocab) + len(vocab_dict['producer'].vocab) + len(vocab_dict['year'].vocab)

        vector = np.zeros(full_len, dtype=np.bool_)
        for token in vocab_dict['actors'].tokenizer.tokenize(self.actors):
            stem = vocab_dict['actors'].get_stem(token)
            idx = vocab_dict['actors'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))

        for token in vocab_dict['country'].tokenizer.tokenize(self.country):
            stem = vocab_dict['country'].get_stem(token)
            idx = vocab_dict['country'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx + len(vocab_dict['actors'].vocab)] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))

        for token in vocab_dict['description'].tokenizer.tokenize(self.description):
            stem = vocab_dict['description'].get_stem(token)
            idx = vocab_dict['description'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx + len(vocab_dict['actors'].vocab) + len(vocab_dict['country'].vocab)] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))

        for token in vocab_dict['name'].tokenizer.tokenize(self.name):
            stem = vocab_dict['name'].get_stem(token)
            idx = vocab_dict['name'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx + len(vocab_dict['actors'].vocab) + len(vocab_dict['country'].vocab) + len(
                    vocab_dict['description'].vocab)] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))

        for token in vocab_dict['producer'].tokenizer.tokenize(self.producer):
            stem = vocab_dict['producer'].get_stem(token)
            idx = vocab_dict['producer'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx + len(vocab_dict['actors'].vocab) + len(vocab_dict['country'].vocab) + len(
                    vocab_dict['description'].vocab) + +len(vocab_dict['name'].vocab)] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))



        for token in vocab_dict['year'].tokenizer.tokenize(self.year):
            stem = vocab_dict['year'].get_stem(token)
            idx = vocab_dict['year'].token_2_idx.get(stem, None)
            if idx is not None:
                vector[idx + len(vocab_dict['actors'].vocab) + len(vocab_dict['country'].vocab) + len(
                    vocab_dict['description'].vocab) + +len(vocab_dict['name'].vocab) + len(
                    vocab_dict['producer'].vocab)] = True
            elif show_unknowns:
                print("Unknown token: {}".format(token))

        return vector

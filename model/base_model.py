class BaseModel:
    def __init__(self, attributes, line):
        if type(line) != tuple:
            raise TypeError('parameter line type is not a tuple')
        if type(attributes) != list:
            raise TypeError('parameter atributes type is not a str')
        for attribute_number, attribute in enumerate(attributes):
            self.__setattr__(attribute, line[attribute_number])

    def getattribute(self, name: str):
        return super().__getattribute__(name)

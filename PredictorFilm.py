import sys

import tensorflow as tf
import tflearn
from sklearn.model_selection import train_test_split
from tflearn.data_utils import to_categorical

from WriterCheckpoint import WriterCheckpoint
from connectDB import *
from vocab import Vocab

logging.basicConfig(format='%(asctime)s.%(msecs)d %(levelname)s in \'%(module)s\' at line %(lineno)d: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO)
actors = [2200]
countrys = [25]
names = [500]
descriptions = [4500]
producers = [800]
years = [60]

starter_learning_rate = [0.1]
count_epoch = [100]

for actor in actors:
    for country in countrys:
        for name in names:
            for description in descriptions:
                for producer in producers:
                    for year in years:
                        full_len = 0
                        connectionMySQL = ConnectionDB()
                        publications = connectionMySQL.get_publications(7000)
                        count_text = len(publications)
                        vocab_dict = {}
                        column_dict_names = ["actors", "country", "description", "name", "producer", "year"]
                        max_vocab_size_dict = {}
                        max_vocab_size_dict["actors"] = actor
                        max_vocab_size_dict["country"] = country
                        max_vocab_size_dict["description"] = description
                        max_vocab_size_dict["name"] = name
                        max_vocab_size_dict["producer"] = producer
                        max_vocab_size_dict["year"] = year

                        for column_name in column_dict_names:
                            vocab_dict[column_name] = Vocab()
                            vocab_dict[column_name].create_vocab(max_vocab_size_dict[column_name],
                                                                 publications, column_name)
                            full_len += len(vocab_dict[column_name].vocab)

                        tweet_vectors = np.zeros(
                            (count_text, full_len),
                            dtype=np.bool_)

                        tweets = []
                        result_final = []
                        logging.info("start convert to vector")
                        for ii, (publication) in enumerate(publications):
                            sys.stdout.write("\rconvert to vector: %d%%" % (ii / len(publications) * 100))
                            sys.stdout.flush()
                            tweet_vectors[ii] = publication.to_vector(vocab_dict)
                            result_final.append((((int)(publication.user_rating)) / 10.0))
                        sys.stdout.write("\rconvert to vector: %d%%" % (100))
                        sys.stdout.flush()
                        labels = np.array(result_final, dtype=np.float32)
                        logging.info("end convert to vector")

                        films_predict = connectionMySQL.get_publications(6000, True)
                        tweet_vectors_predict = np.zeros(
                            (len(films_predict), full_len),
                            dtype=np.bool_)
                        logging.info("start convert to vector test")
                        tweets_predict = []
                        result_final_predict = []
                        for ii, (publication) in enumerate(films_predict):
                            sys.stdout.write(
                                "\rconvert to vector test: %d%%" % (ii / len(films_predict) * 100))
                            sys.stdout.flush()
                            tweet_vectors_predict[ii] = publication.to_vector(vocab_dict)
                            result_final_predict.append((((int)(publication.user_rating)) / 10.0))
                        sys.stdout.write("\rconvert to vector test: %d%%" % 100)
                        sys.stdout.flush()
                        labels_predict = np.array(result_final_predict, dtype=np.float32)
                        logging.info("end convert to vector")

                        len_kyrs = 2
                        n = full_len
                        m = len_kyrs
                        r = pow(n / m, 1 / 3)
                        k1 = (int)(m * pow(r, 2))
                        k2 = (int)(m * r)
                        print('count of neurons in the input layer: ', n)
                        print('count of neurons in 1 layer: ', k1)
                        print('count of neurons in 2 layer: ', k2)
                        print('count of neurons in the output layer: ', m)

                        print('Start project')

                        X = tweet_vectors
                        y = to_categorical(labels, 2)
                        for ii, (label) in enumerate(labels):
                            y[ii][0] = label
                            y[ii][1] = 1 - label
                        X_train, X_test2, y_train, y_test2 = train_test_split(X, y, test_size=0.1)

                        X_predict = tweet_vectors_predict
                        y_predict = to_categorical(labels_predict, 2)
                        for ii, (label) in enumerate(labels_predict):
                            y_predict[ii][0] = label
                            y_predict[ii][1] = 1 - label


                        def build_model(learning_rate=0.1):
                            tf.reset_default_graph()
                            x = tflearn.input_data([None, full_len])
                            net = tflearn.batch_normalization(x)
                            net1 = tflearn.fully_connected(net, k1, activation='LeakyReLU')
                            net2 = tflearn.batch_normalization(net1)
                            net3 = tflearn.fully_connected(net2, k2, activation='LeakyReLU')
                            net4 = tflearn.fully_connected(net3, len_kyrs, activation='softmax')
                            regression = tflearn.regression(
                                net4,
                                optimizer='sgd',
                                learning_rate=learning_rate,
                                loss='categorical_crossentropy')
                            model = tflearn.DNN(net4, tensorboard_dir='/tmp/tflearn_logs8/',
                                                tensorboard_verbose=0)
                            return model


                        model = build_model(starter_learning_rate)
                        wc = WriterCheckpoint()
                        wc.save_checkpoint()
                        wc.save_mpdel(model)
                        model.fit(X_train, y_train, validation_set=0.2, show_metric=True, batch_size=64,
                                  n_epoch=count_epoch,
                                  run_id=wc.name_model)

                        result = model.predict(X_predict)
                        for ii, (publication) in enumerate(films_predict):
                            sys.stdout.flush()
                            connectionMySQL.update_rating(publication.id, str(result[ii][0]))
                        wc.write_to_word(y_predict, result, name, description, starter_learning_rate,
                                         count_epoch)
